## Magic Postman 简易Chrome插件

**提供GET，POST，PUT，DELETE类型接口调试**

### 使用方法

1.克隆或下载代码到本地
```
git clone https://gitlab.com/qiaen/magic-postman.git
```

2.通过浏览器菜单面板依次打开 **更多工具** - **拓展程序**

3.启用勾选“**开发者模式**”

4.**加载已解压的拓展程序**

5.选择“**magic-postman**”

6.点击右上角“**magic-postman**”图标或鼠标右击“**接口调试工具**”即可
