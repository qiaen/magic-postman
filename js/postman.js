/* cookie */
axios.defaults.withCredentials = true
let app = new Vue({
	el: "#main",
	data: {
		/* 支持的方式 */
		methods: ['GET', 'POST', 'PUT', 'DELETE'],
		/* 打开的Tabs */
		editableTabs: [],
		editableTabsValue: '1',
		/* 初始化显示api配置 */
		config: {
			method: 'GET',
			name: '',
			url: '',
			reqDisplayType: 'key',
			params: [{
				key: '',
				value: ''
			}],
			data: '{ "":"" }',
			response: '{}',
			loading: false
		},
		/* 弹窗控制层 */
		dialog: {
			group: false,
			introduce: false
		},
		/* Api分组相关 开始 */
		groupList: [],
		selectGroup: '',
		tabIndex: 0,
		userApiGroup: [{ name: '常用', list: [] }],
		newGroupName: '',
		apisObject: {},
		/* 组管理2，保存1 */
		groupFuncType: 0,
		/* Api分组相关结束 */
	},
	methods: {
		/* 发送请求 */
		send(item) {
			if (!item.url) { return }
			if(!item.url.includes('http://')&&!item.url.includes('htts://')){
				return this.$message.warning('链接好像不怎么对~')
			}
			let config = { ...item }
			let temp = {}
			if (item.reqDisplayType === 'key') {
				for (let m of item.params) {
					if (m.key !== '') {
						temp[m.key] = m.value
					}
				}
			} else {
				temp = JSON.parse(item.data)
			}
			/* 删除key为空的对象 */
			for (let i in temp) {
				if (i === '') {
					delete temp[i]
				}
			}
			/* 处理Axios，get方法使用params，post方法用data */
			config.data = temp
			if (config.method === "GET") {
				config.params = config.data
				delete config.data
			}
			delete config.reqDisplayType
			delete config.response
			console.log(config)
			item.loading = true
			axios(config).then(res => {
				item.response = res.data
				item.loading = false
			}).catch(err => {
				let res = err.response
				item.response = res.data || { status: res.status, statusText: res.statusText }
				console.log(err.response)
				item.loading = false
			})
		},
		/* tabs 增加tab和删除tab操作 */
		handleTabsEdit(targetName, action, api) {
			if (action === 'add') {
				let newTabName = 'New Tab ' + (++this.tabIndex + '')
				if (api) {
					let temp = { ...api }
					temp.name = newTabName
					temp.response = '{}'
					this.editableTabs.push(temp)
				} else {
					this.config.name = newTabName
					this.editableTabs.push({ ...this.config })
				}
				this.editableTabsValue = newTabName
			}
			if (action === 'remove') {
				let tabs = this.editableTabs;
				if (tabs.length <= 1) {
					return this.$message.warning('做人留一线，日后好相见！')
				}
				let activeName = this.editableTabsValue
				if (activeName === targetName) {
					tabs.forEach((tab, index) => {
						if (tab.name === targetName) {
							let nextTab = tabs[index + 1] || tabs[index - 1]
							if (nextTab) {
								activeName = nextTab.name
							}
						}
					});
				}
				this.editableTabsValue = activeName
				this.editableTabs = tabs.filter(tab => tab.name !== targetName)
			}
		},
		/* 键对值操作，自动新增一条空行， []params, action: (1新增，0删除)，index: 操作的行*/
		editParams(items, action, index) {
			let length = items.length
			if (action) {
				let empty = items.filter(m => m.key === '')
				/* 有空key就不新增行 */
				if (!empty.length) {
					items.push({ key: '', value: '' })
				}
			} else {
				/* 如果是最后一个不删除，只清空 */
				if (length === 1) {
					items[0].key = items[0].value = ''
				} else {
					items.splice(index, 1)
				}
			}
		},
		/* 处理粘贴操作 */
		paste(event, item) {
			let clipboardData = event.clipboardData || window.clipboardData
			if (clipboardData) {
				let txt = clipboardData.getData('Text')
				txt = txt.replace(/\/n/g, '').replace(/\/r/g, '')
				if (txt) {
					try {
						/* 只处理是对象的情况，使用eval是方式{"key": ''}, 中key没有引号包裹无法转换 */
						txt = eval('(' + txt + ')')
						let arr = []
						if (txt.constructor === Object) {
							for (let i in txt) {
								arr.push({
									key: i,
									value: txt[i]
								})
							}
							setTimeout(() => {
								item.params = arr
							}, 100)
						}
					} catch (e) {}
				}
			}
		},
		/* str要转化的内容, type：函数接收的类型 */
		str2json(str, type) {
			try {
				let m = JSON.parse(str)
				if (type === 'Object') {
					return m.constructor === Object ? m : ''
				} else if (type === 'Array') {
					return m.constructor === Array ? m : ''
				} else {
					return m
				}
			} catch (e) {
				console.log(e)
				return ''
			}
		},
		/* 自定义携带的cookie待开发 */
		setCookie() {},
		/* 保存到分组 */
		saveAs() {
			this.handleCommand(this.selectGroup, this.tempApiConfig)
		},
		/* 另存下拉点击事件 name：组名，api：当前展开的api*/
		handleCommand(name, api) {
			if (name === '新分组') {
				this.groupFuncType = 1
				this.dialog.group = true
				this.tempApiConfig = api
			} else {
				if (!api.url) {
					return this.$message.warning('无法另存空链接')
				}
				// 处理当前Api信息
				let item = { ...api }
				delete item.response
				delete item.name
				delete item.loading
				item.randomIndex = Math.random()
				let oldApis = this.apisObject[name] || []
				oldApis.push(item)
				localStorage.setItem('_UserApis_' + name, JSON.stringify(oldApis))
				this.getGroupList()
				this.mapAllGroup()
				this.dialog.group = false
			}
		},
		/* 新建或删除分组 item：分组名，index：行号*/
		createNewGroup(item, index) {
			if (item) {
				/* 删除操作 */
				localStorage.clear('_UserApis_' + item)
				if (item === '常用') {
					this.$message.success('组内所有api已清空')
					this.apisObject['常用'] = []
				} else {
					this.groupList.splice(index, 1)
				}
			} else {
				/* 创建 */
				let ngm = this.newGroupName
				let gl = this.groupList
				if (!ngm) { return }
				if (gl.includes(ngm) || ngm === '常用' || ngm === '新分组') {
					return this.$message.warning(`组名：${ngm} 已存在，或不可用`)
				} else {
					this.groupList.push(ngm)
				}
				this.selectGroup = ngm
				this.newGroupName = ''
			}
			localStorage.setItem('_UserGroups', JSON.stringify(this.groupList))
			this.getGroupList()
		},
		/* 从本地读取组并赋值 */
		getGroupList() {
			let groups = this.str2json(localStorage.getItem('_UserGroups'), 'Array')
			this.groupList = groups || []
		},
		/* 根据组名获取所有的api */
		getApisByGroup(name) {
			let apis = this.str2json(localStorage.getItem('_UserApis_' + name), 'Array')
			if (apis) {
				this.apisObject[name] = apis
			}
		},
		/* 遍历所有组并获取api */
		mapAllGroup() {
			this.getApisByGroup('常用')
			this.groupList.forEach(item => {
				this.getApisByGroup(item)
			})
		}
	},
	computed: {},
	created() {
		/* 创建一个tab */
		this.handleTabsEdit(false, 'add')
		/* 获取所有组 */
		this.getGroupList()
		/* 遍历所有组并获取api */
		this.mapAllGroup()
	}
})